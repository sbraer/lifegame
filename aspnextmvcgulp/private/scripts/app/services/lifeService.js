﻿define(["underscore"], function (_) {
    return ['$interval', function ($interval) {
        var tables = { table: [], maxWidth: 0, started: false };
        var numberOfColumns, numberOfRows, numberOfCells;
        var interval;
        var cacheArrayZeroFill;

        var createTable = function (numColumns, numRows, pattern) {
            if (!tables.started) {
                numberOfColumns = numColumns;
                numberOfRows = numRows;
                numberOfCells = numberOfRows * numberOfColumns;
                tables.maxWidth = numberOfColumns * 20;
                var buff = new Array(numberOfCells);
                cacheArrayZeroFill = new Array(numberOfCells);

                for (var i = 0; i < numberOfCells; i++) {
                    buff[i] = { id: i, state: false };
                    cacheArrayZeroFill[i] = 0;
                }

                if (!_.isUndefined(pattern)) {
                    var maxX = 0, maxY = 0;
                    _.each(pattern, function (value, key) {
                        if (value[1] > maxY) { maxY = value[1]; }
                        if (value[0] > maxX) { maxX = value[0]; }
                    });

                    maxX++; maxY++;

                    if (maxX > numberOfColumns) {
                        alert("Patter width error")
                        return;
                    }

                    if (maxY > numberOfRows) {
                        alert("Patter height error")
                        return;
                    }

                    var centerX = Math.floor(numberOfColumns / 2 - maxX / 2);
                    var centerY = Math.floor(numberOfRows / 2 - maxY / 2);

                    _.each(pattern, function (value, key) {
                        var y = centerY + value[1];
                        var x = centerX + value[0];

                        buff[x + y * numberOfColumns].state = true;
                    });
                }

                tables.table = [].concat(buff);
            }
        }

        var singleStep = function () {
            if (!tables.started) {
                elaborate();
            }
        }

        var elaborate = function () {
            var cacheTable = checkAllCell();
            var tableTemp = tables.table;

            for (var i = 0; i < numberOfCells; i++) {
                var around = cacheTable[i];
                var cell = tableTemp[i];
                if (!cell.state) {
                    if (around == 3) {
                        cell.state = true;
                    }
                }
                else {
                    if (around != 2 && around != 3) {
                        cell.state = false;
                    }
                }
            }
        }

        var checkAllCell = function () {
            var cacheTable = cacheArrayZeroFill.concat([]);

            for (var y = 0; y < numberOfRows; y++) {
                for (var x = 0; x < numberOfColumns; x++) {
                    var cell = tables.table[x + y * numberOfColumns];
                    if (cell.state) {
                        if (y > 0) {
                            if (x > 0) {
                                cacheTable[x - 1 + (y - 1) * numberOfColumns]++;
                            }
                            cacheTable[x + (y - 1) * numberOfColumns]++;
                            if (x < numberOfColumns - 1) {
                                cacheTable[x + 1 + (y - 1) * numberOfColumns]++;
                            }
                        }
                        if (x > 0) {
                            cacheTable[x - 1 + y * numberOfColumns]++;
                        }
                        if (x < numberOfColumns - 1) {
                            cacheTable[x + 1 + y * numberOfColumns]++;
                        }
                        if (y < numberOfRows - 1) {
                            if (x > 0) {
                                cacheTable[x - 1 + (y + 1) * numberOfColumns]++;
                            }
                            cacheTable[x + (y + 1) * numberOfColumns]++;
                            if (x < numberOfColumns - 1) {
                                cacheTable[x + 1 + (y + 1) * numberOfColumns]++;
                            }
                        }
                    }
                }
            }
            return cacheTable;
        }

        var start = function (ms) {
            if (!tables.started) {
                tables.started = true;
                interval = $interval(function () {
                    elaborate();
                }, ms);
            }
        }

        var stop = function () {
            if (tables.started) {
                $interval.cancel(interval);
                tables.started = false;
            }
        }

        var speedChanged = function (speed) {
            if (tables.started) {
                stop();
                start(speed);
            }
        }

        var setPattern = function (data) {
            if (!tables.started) {
                createTable(numberOfColumns, numberOfRows, data);
            }
        }

        return {
            getTables: tables,
            createTable: createTable,
            singleStep: singleStep,
            start: start,
            stop: stop,
            setPattern: setPattern,
            speedChanged: speedChanged
        }
    }]
});