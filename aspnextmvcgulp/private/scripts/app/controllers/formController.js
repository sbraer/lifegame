﻿define(['speedValueList'], function (speedValueList) {
    return ['lifeService', 'patternService', function (lifeService, patternService) {
        var self = this;

        self.numberOfColumns = 40;
        self.numberOfRows = 30;
        self.numberOfCells = 0;
        self.widthMainTable = 300;
        self.matchPattern = new RegExp("^[\s]*[2-8][0-9][\s]*$|^[\s]*90$[\s]*$");
        self.speedValues = speedValueList.collection;
        self.speed = speedValueList.collection[1].value;
        patternService.remoteRequest().then(function (response) {
            self.patternList = response.data;
            self.reset();
        });
        self.tables = lifeService.getTables;

        self.singleStep = function () { lifeService.singleStep(); }
        self.start = function () { lifeService.start(self.speed); }
        self.stop = function () { lifeService.stop(); }
        self.speedChanged = function () { lifeService.speedChanged(self.speed); }

        self.reset = function () {
            self.pattern = self.patternList[0];
            self.createTableForm();
        }

        self.patternChanged = function () {
            var data = self.pattern.Data;
            lifeService.setPattern(data);
        }

        self.createTableForm = function () {
            lifeService.createTable(self.numberOfColumns, self.numberOfRows);
            self.widthMainTable = self.numberOfColumns * 20;
        }
    }]
});
