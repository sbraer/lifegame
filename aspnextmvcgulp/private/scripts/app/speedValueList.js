﻿define(function () {
    var collection = [
        { label: "Slow", value: 300 },
        { label: "Medium", value: 120 },
        { label: "Fast", value: 30 }];

    return {
        collection: collection
    }
});