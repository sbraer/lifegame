﻿require.config({
    paths: {
        angular: ['//cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.7/angular.min','/scripts/vendor/angular.min'],
        underscore: ['//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min','/scripts/vendor/underscore.min']
},
    shim: {
        angular: {
            exports: 'angular'
        }
    },
    deps: ['app']
});