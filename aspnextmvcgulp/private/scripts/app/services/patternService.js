﻿define([], function () {
    return ["$http", function ($http) {
        return {
            remoteRequest: function () { return $http.get('/api/patterns'); }
        }
    }]
});