/// <binding AfterBuild='amdOpt, css1, startjs' />
/*
This file in the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkId=518007
*/

var gulp = require('gulp');
var uglify = require("gulp-uglify");
var concat = require("gulp-concat");
var rename = require("gulp-rename");
var minifycss = require("gulp-clean-css");
var amdOptimize = require('gulp-amd-optimizer');
var sourcemaps = require('gulp-sourcemaps');

var amdConfig = {
    baseUrl: './private/scripts/app/',
    path: {
        'lib': './private/scripts/app/'
    },
    exclude: [
        'jQuery', 'angular', 'underscore'
    ]
};

gulp.task('amdOpt', function () {
    return gulp.src('./private/scripts/app/*.js', { base: amdConfig.baseUrl })
        .pipe(sourcemaps.init())
    .pipe(amdOptimize(amdConfig))
    .pipe(concat('app.js'))
    .pipe(uglify())
        .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./Scripts/'));
});

gulp.task('css1', function () {
    console.log("start css1");
    gulp.src(["./private/css/*.css"])
    .pipe(concat("styles.css"))
    .pipe(minifycss())
    .pipe(gulp.dest("./css/"));
});

gulp.task('startjs', function () {
    console.log('startjs!');

    gulp.src(["./wwwroot/lib/angular/angular.js"])
    .pipe(concat("angular.min.js"))
    .pipe(uglify())
    .pipe(gulp.dest("./Scripts/vendor/"));

    gulp.src(["./wwwroot/lib/requirejs/require.js"])
    .pipe(concat("require.min.js"))
    .pipe(uglify())
    .pipe(gulp.dest("./Scripts/vendor/"));

    gulp.src(["./wwwroot/lib/underscore/underscore.js"])
    .pipe(concat("underscore.min.js"))
    .pipe(uglify())
    .pipe(gulp.dest("./Scripts/vendor/"));
});

gulp.task('watch1', function () {
    return gulp.watch('./private/scripts/app/*.js', ['amdOpt']);
});

gulp.task('watch2', function () {
    return gulp.watch('./private/css/*.css', ['css1']);
});
