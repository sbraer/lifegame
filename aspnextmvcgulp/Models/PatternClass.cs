﻿using System;
using System.Collections.Generic;

namespace aspnextmvcgulp.Models
{
    public class PatternClass
    {
        public string Name { get; set; }
        public List<int[]> Data { get; set; }
    }
}