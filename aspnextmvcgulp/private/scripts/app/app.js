﻿require(["angular", "controllers/formController", "controllers/gridController", "services/lifeService", "services/patternService"],
    function (angular, formController, gridController, lifeService, patternService) {
    angular
        .module('requireModule', [])
        .service('lifeService', lifeService)
        .service('patternService', patternService)
        .controller('formController', formController)
        .controller('gridController', gridController)
    ;
    angular.bootstrap(document, ['requireModule']);
});
